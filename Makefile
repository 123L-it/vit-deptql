###############################
# Common defaults/definitions #
###############################

comma := ,

# Checks two given strings for equality.
eq = $(if $(or $(1),$(2)),$(and $(findstring $(1),$(2)),\
								$(findstring $(2),$(1))),1)


###########
# Aliases #
###########

setup: cargo.setup


fmt: cargo.fmt


lint: cargo.lint


test: cargo.test


##################
# Cargo commands #
##################

# Setup required rust components.
#
# Usage:
#	make cargo.setup

cargo.setup:
	rustup component add rustfmt clippy
	cargo install cargo-insta

# Format Rust sources with rustfmt.
#
# Usage:
#	make cargo.fmt [check=(no|yes)]

cargo.fmt:
	cargo +nightly fmt --all $(if $(call eq,$(check),yes),-- --check,)


# Lint Rust sources with Clippy.
#
# Usage:
#	make cargo.lint

cargo.lint:
	cargo lint


####################
# Testing commands #
####################

# Run Rust tests of project crates.
#
# Usage:
#	make test.cargo [review=(no|yes)]

cargo.test:
ifeq ($(shell cargo install --list | grep cargo-insta),)
		cargo install cargo-insta
endif
ifeq ($(review),yes)
		cargo test-review
endif
		cargo test


##################
# .PHONY section #
##################

.PHONY: book fmt lint test \
		cargo.fmt cargo.lint cargo.test
