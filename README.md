# vit-graphql

## Requirements
- Rust ([rustup][rust-install])

## Installation

```bash
make setup
```

## Usage

```bash
cargo run
```

 [rust-install]: https://www.rust-lang.org/tools/install
